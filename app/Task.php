<?php

namespace App;


use PDO;

class Task
{
    protected $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }
    
    
    public function displayTask(){
        try{

            $cmd = $this->connection->prepare("SELECT * from task order by priority,name");
            $cmd->execute();
            $list = $cmd->fetchAll(PDO::FETCH_ASSOC);
            

            $cmd = null;

            return json_encode($list);

        }catch(\PDOException $c){
            echo $c->getMessage();

        }
    }
    public function viewTask($id){
        try{

            $cmd = $this->connection->prepare("SELECT * FROM task where id = ? limit 1");
            $cmd->bindParam(1,$id);
            $cmd->execute();
            $cmd->execute();
            $list = $cmd->fetch(PDO::FETCH_ASSOC);


            $cmd = null;

            return $list;

        }catch(\PDOException $c){
            echo $c->getMessage();

        }
    }
    public function deleteTask($id){
        try{
            $cmd = $this->connection->prepare("DELETE from task where id = ?");
            $cmd->bindParam(1, $id);
            $cmd->execute();
            $message = array('success'=>true);

            return json_encode($message);

        }catch(\PDOException $ex){

        }
    }
    public function addTask($name,$priority){
        try{

            $cmd = $this->connection->prepare("INSERT INTO task ( name,priority ) VALUES (?,?)");
            
            $cmd->bindParam(1, $name ,PDO::PARAM_STR);
            $cmd->bindParam(2, $priority,PDO::PARAM_INT);

            $cmd->execute();
            $message = array('msg'=>"success");
            $cmd=null;
            return json_encode($message);

        }catch(\PDOException $ex){
            $message = array('msg'=>$ex->getMessage());
            return json_encode($message);
        }
    }
    
    public function editTask($id){
        try{

            $cmd = $this->connection->prepare("SELECT * FROM task where id = ? limit 1");
            $cmd->bindParam(1,$id);
            $cmd->execute();
            $cmd->execute();
            $list = $cmd->fetch(PDO::FETCH_ASSOC);


            $cmd = null;

            return $list;

        }catch(\PDOException $c){
            echo $c->getMessage();

        }
    }
    
    public function updateTask($taskname,$priority,$id){
        try{
            $cmd = $this->connection->prepare("UPDATE task set name = ?, priority = ?
                                               where id = ?");
            
           
            $cmd->bindParam(1,$taskname,PDO::PARAM_STR);
            $cmd->bindParam(2,$priority,PDO::PARAM_INT);
            $cmd->bindParam(3,$id,PDO::PARAM_INT);
            $cmd->execute();
            $rows = $cmd->rowCount();

            if($rows == 1){
                $message =  array('success'=>true);
            }else{
                $message =  array('success'=>false);
            }

            return json_encode($message);

        }catch(\PDOException $ex){
            echo $ex->getMessage();
        }

    }
    
    public function completeTask($status,$id){
        try{
            $cmd = $this->connection->prepare("UPDATE task set status = ?
                                               where id = ?");
            
            $cmd->bindParam(1,$status,PDO::PARAM_INT);
            $cmd->bindParam(2,$id,PDO::PARAM_INT);
            $cmd->execute();
            $rows = $cmd->rowCount();

            if($rows == 1){
                $message =  array('success'=>true);
            }else{
                $message =  array('success'=>false);
            }

            return json_encode($message);

        }catch(\PDOException $ex){
            echo $ex->getMessage();
        }
    }
    
    public function totalTask(){
        try{

            $cmd = $this->connection->prepare("SELECT count(*)  FROM task");
           
            $cmd->execute();
            $list = $cmd->fetchColumn();


            $cmd = null;

            return $list;

        }catch(\PDOException $c){
            echo $c->getMessage();

        }
    }
    public function totalCompletedTask(){
        try{
            $status = 1;
            $cmd = $this->connection->prepare("SELECT count(*)  FROM task where status = ?");
            $cmd->bindParam(1,$status,PDO::PARAM_INT);
            $cmd->execute();
            $list = $cmd->fetchColumn();


            $cmd = null;

            return $list;

        }catch(\PDOException $c){
            echo $c->getMessage();

        }
    }
}
