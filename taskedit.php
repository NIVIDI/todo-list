<?php
require 'vendor/autoload.php';
require_once 'config.php';

use App\Task;

$task = new Task($condb);

if(isset($_GET['edittask'])){
    $taskid = filter_input(INPUT_GET, 'edittask', FILTER_VALIDATE_INT);
    $row = $task->editTask($taskid);
}
if(isset($_POST['updatetask']) && $_POST['updatetaskbtn'] == 'Update Task'){

    $taskname  = filter_input(INPUT_POST, 'taskname', FILTER_SANITIZE_STRING);
    $priority = filter_input(INPUT_POST, 'priority', FILTER_VALIDATE_INT);
    $id = filter_input(INPUT_POST, 'updatetask', FILTER_VALIDATE_INT);
    
    $update = $task->updateTask($taskname,$priority,$id);
    unset($_SESSION['message']);
    header('Location: /');    
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Task</title>
    <link rel="stylesheet" href="/css/app.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.13/fc-3.2.2/fh-3.1.2/r-2.1.0/sc-1.4.2/datatables.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
</head>
<body>

    <div class="container">
        <div style="margin-top:50px;">
            <a href="/" >Back</a>
        </div>
        <div class="containerform" style="margin-top:100px;">
            <form action="taskedit.php" method="post">
                <label for="taskname">Task Name</label>
                <input type="text" id="taskname" required name="taskname" value="<?=$row['name'];?>">
                <input type="hidden" id="updatetask" name="updatetask" value="<?=$row['id'];?>">

                <label for="country">Priority</label>
                <select id="priority" name="priority" required>
                     <?php foreach (range(1, 10) as $number):?>
                          <?php if($number == $row['priority']):?>
                            <option value="<?=$number?>" selected><?=$number?></option>   
                          <?php else: ?>
                               <option value="<?=$number?>"><?=$number?></option>
                          <?php endif;?> 
                     <?php endforeach; ?>
                </select>


                <input type="submit" value="Update Task" id="updatetask" name="updatetaskbtn" style="margin-top:10px;">
            </form>
        </div>
    </div>

</body>
</html>
