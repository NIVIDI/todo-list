CREATE TABLE `task` (
                        `id` INT(10) NOT NULL AUTO_INCREMENT,
                        `name` TEXT NOT NULL COLLATE 'utf8mb4_general_ci',
                        `priority` TINYINT(3) NOT NULL DEFAULT '0',
                        `status` TINYINT(3) NOT NULL DEFAULT '0',
                        PRIMARY KEY (`id`) USING BTREE
)
    COLLATE='utf8mb4_general_ci'
    ENGINE=InnoDB
    AUTO_INCREMENT=5
;
