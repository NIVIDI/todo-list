<?php
require 'vendor/autoload.php';
require_once 'config.php';

use App\Task;

$task = new Task($condb);

if(isset($_GET['deletetask'])){
    $taskid = filter_input(INPUT_GET, 'deletetask', FILTER_VALIDATE_INT);
    $delete = json_decode($task->deleteTask($taskid),true);
    
    $_SESSION['message'] = "Successfully deleted.";
    header('Location: /');
    exit;
}
if(isset($_POST['formaddtask']) && $_POST['addtaskbtn'] == 'Add Task'){
    
    $taskname  = filter_input(INPUT_POST, 'taskname', FILTER_SANITIZE_STRING);
    $priority = filter_input(INPUT_POST, 'priority', FILTER_VALIDATE_INT);
    
    $save = $task->addTask($taskname,$priority);

    $_SESSION['message'] = "Successfully added new task.";

    header('Location: /');
    exit;
}

if(isset($_GET['completetask'])){
    $taskid = filter_input(INPUT_GET, 'completetask', FILTER_VALIDATE_INT);
    $complete = json_decode($task->completeTask(1,$taskid),true);

    $_SESSION['message'] = "Task successfully completed.";
    header('Location: /');
    exit;
}

?>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/app.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.13/fc-3.2.2/fh-3.1.2/r-2.1.0/sc-1.4.2/datatables.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        var table = $('#table-todo').DataTable( {
            scrollCollapse: false,
            paging:         false,
            "bInfo" : false,
            "columnDefs": [
              { "orderable": false, "targets": [2,3] }
            ]
        } );
    });
    </script>
    <style>
      .dataTables_filter {
        display: none;
      }
    </style>
</head>
<body>

<div class="container">
    <?php
            if (!empty($_SESSION['message'])) {
                $message = $_SESSION['message'];
                if (isset($_SESSION['message'])) {
                  unset($_SESSION['message']);
                }

                echo '<div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Hooray!</strong> '.$message.'
                    </div>';
                
            }
        ?>
        
   <div class="containerform" style="margin-top:100px;">
        <form action="index.php" method="post">
           <label for="taskname">Task Name</label>
           <input type="text" id="taskname" name="taskname" required placeholder="">
           <input type="hidden" id="formaddtask" name="formaddtask">
           
           <label for="country">Priority</label>
           <select id="priority" name="priority" required>
               <option ></option>
               <?php foreach (range(1, 10) as $number):?>
                   <option value="<?=$number?>"><?=$number?></option>
                <?php endforeach; ?>    
               
           </select>
           

           <input type="submit" value="Add Task" id="addtask" name="addtaskbtn"  style="margin-top: 10px;">
       </form>
   </div> 
    
    <table id="table-todo" class="task-table" style="margin-top:100px;">
        <thead>
        <tr>
            
            <th>Task Name</th>
            <th>Priority</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach(json_decode($task->displayTask(),true) as $t) :?>
            <tr>
                <?php
                    $completeTasksButton = '';
                    if ($t['status'] != 1) {
                       $completeTasksButton = '| <a href="index.php?completetask='.$t['id'].'">Complete Task</a>';
                    }
                ?>
                <td><?=$t['name'];?></td>
                <td><?=$t['priority'];?></td>
                <td><?= $t['status'] == 1 ? 'Completed' : '';?></td>
                <td><a href="taskview.php?id=<?=$t['id'];?>">View</a> | <a href="index.php?deletetask=<?=$t['id'];?>"  onclick="if (confirm('Delete selected item?')){return true;}else{event.stopPropagation(); event.preventDefault();};">Delete</a> | <a href="taskedit.php?edittask=<?=$t['id'];?>">Edit</a>  <?= $completeTasksButton;?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <br>
    <small>Total Tasks: <b><?=$task->totalTask();?></b></small>
      <br>  
      <br>  
     <small>Total Completed Task: <b><?=$task->totalCompletedTask();?></b> </small>
</div>
</body>
</html>


