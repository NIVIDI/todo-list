<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
//error_reporting(E_ALL);
error_reporting(0);
session_start();

use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__ );
$dotenv->safeLoad();

$host = $_ENV['DB_HOST'];
$db = $_ENV['DB_DATABASE'];
$user = $_ENV['DB_USERNAME'];
$password = $_ENV['DB_PASSWORD'];


DEFINE("HOST","mysql:host=$host;dbname=$db;charset=utf8mb4");
DEFINE("USER",$user);
DEFINE("PASSWORD",$password);

try{
    $condb = new PDO(HOST,USER,PASSWORD);
    $condb->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
   
}catch(PDOException $e){
    die("Unable to connect to the database");
}



?>
